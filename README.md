This project uses Python 3 and is the first ever Python project I've ever worked on :)

Requires BeautifulSoup - can be installed with `pip install beautifulsoup4`

Takes tracking number as a command line input.

You'll have to supply your own notifying code - in my case it calls a helper on my website that sends a text message, but I've redacted everything about that.

This was mainly created as a project for personal use but feel free to use the code however you please.
