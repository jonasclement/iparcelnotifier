#!/usr/bin/env python3.4

import sys
import os
import urllib.parse
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup

# Check commandline arguments
if len(sys.argv) != 2:
    print("Usage: python3.4 notifier.py <trackingnumber>")
    exit(1)

FILENAME = os.path.dirname(os.path.abspath(__file__)) +  "/last_snapshot.html"

tracking_number = sys.argv[1]
url = "https://tracking.i-parcel.com/"
data = {"TrackingNumber": tracking_number}
data = urllib.parse.urlencode(data)
full_url = "%s?%s" % (url, data)

# Fetch tracking page
with urlopen(full_url) as ups_response:
    soup = BeautifulSoup(ups_response.read(), "html.parser")
    # Parse the HTML a bit to only get the <div>.mainContainer and clean up a bit - reduces risk of false positives.
    main_container = soup.html.body.find("div", {"class": "mainContainer"})
    main_container.find("div", {"class": "blurb"}).decompose()
    main_container.find("div", {"class": "footer"}).decompose()
    main_container = main_container.prettify(encoding="utf-8")

    # Create file if it doesn't exist
    try:
        file = open(FILENAME, "x")
        file.close()
    except Exception:
        # File already exists, all good
        pass

    # Compare snapshot to earlier file
    with open(FILENAME, mode="rb") as file:
        file_content = file.read()
        if main_container == file_content:
            # Nothing to do as nothing has changed - no reason to continue execution
            exit(2)
        else:
            # Notify via SMS!
            url = "REDACTED"
            data = {"src": "Notifier",
                    "dst": "REDACTED",
                    "msg": "Something changed on your tracking page!"}
            data = urllib.parse.urlencode(data)
            full_url = "%s?%s" % (url, data)

            request = Request(full_url)
            request.add_header("X-REDACTED", 1)
            request.add_header("Authorization", "REDACTED")
 
            with urlopen(request) as sms_response:
                # Check response
                # TODO: Actual error handling?
                response_code = sms_response.getcode()

    # Save UPS response snapshot to file
    with open(FILENAME, mode="wb") as file:
        file.write(main_container)
        file.flush()

exit(0)
